# flutter_flame_isometric_poc



## Getting started
This is a simple POC (proof of concept) demo of an isometric game in Flutter, a 2D game engine. Also, not all of the actual verification has been done here, but only some of the key elements such as the loading of the iosmetric map, and whether or not there is any corresponding production. isometric map or slicing tool ([Tile map Editor](https://www.mapeditor.org)), how the player character in the map for animation or correct movement or walking, but did not verify the particle engine, physical collision effects, has been the map obstacle bypass (although the code does not show, but already have the relevant ideas, can be based on the tiles matrix to determine the specific obstacles to bypass the rules or pop-up alert)

Then figure out the process of flutter flame isometric, found the flame 2d game engine and isometric class game some resources. Inevitably, flame has a tool called [bonfire]() that helps developers make it easier to develop flame 2d games. It comes with a variety of application-friendly tools and encapsulates better, more intuitive api's, such as the Joystick, for manipulating worlds and slices.Because of time constraints, I haven't spent much time modifying it to work with isometric, but if I need to, I can spend some time optimizing it for isometric compatibility.




## Name
Flutter Flame Isometric Game POC





## Installation
The project supports iOS/macOS/Android/Web/Windows, but because of time constraints I only tested it on iOS/macOS, if I have the time, I will test it on other platforms as well.


Installation conditions

1. Flutter development environment (must).
2. iOS development environment (optional).
3. Android development environment (optional).
4. chrome(optional); 5. macOS/Windows operating system
5. macOS/Windows operating system(must).
Others

## Usage

This is a demo, only reference value, no engineering value



## Roadmap




## License
MIT

## Project status
demo


* There is a bug in the dragging behavior, you can only drag the topleft of the map with one finger, but other positions are problematic. However, this should be a widget click point range exception that can be modified.


### References

* [https://opengameart.org/content/isometric-64x64-medieval-building-tileset](https://opengameart.org/content/isometric-64x64-medieval-building-tileset)
* [https://opengameart.org/content/isometric-painted-game-assets](https://opengameart.org/content/isometric-painted-game-assets)
* [https://docs.flame-engine.org/latest/tutorials](https://docs.flame-engine.org/latest/tutorials)
* [https://github.com/flame-engine/flame_example](https://github.com/flame-engine/flame_example)
* [https://stackoverflow.com/questions/70681215/how-should-you-do-matrix-transformations](https://stackoverflow.com/questions/70681215/how-should-you-do-matrix-transformations)
* [https://pub.dev/packages/flame_isometric/install](https://pub.dev/packages/flame_isometric/install)
* [https://flutterawesome.com/isometric-map-generation-using-flame-flutters-game-engine/](https://flutterawesome.com/isometric-map-generation-using-flame-flutters-game-engine/)
* [https://www.kodeco.com/37130129-building-games-in-flutter-with-flame-getting-started](https://www.kodeco.com/37130129-building-games-in-flutter-with-flame-getting-started)
* [https://blog.codemagic.io/flutter-flame-game-development/](https://blog.codemagic.io/flutter-flame-game-development/)
* [https://www.mapeditor.org](https://www.mapeditor.org)
