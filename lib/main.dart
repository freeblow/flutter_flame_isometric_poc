import 'package:flame/flame.dart';
import 'package:flame/game.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'isometric/common/navigation_keys.dart';
import 'isometric/isometric_game.dart';
import 'isometric/isometric_tile_map.dart';
import 'isometric/isometric_tile_multi_dimension_map.dart';

void main() async{

  WidgetsFlutterBinding.ensureInitialized();
  if (!kIsWeb) {
    await Flame.device.setLandscape();
    await Flame.device.fullScreen();
  }
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
   MyApp({super.key});

  final _game = IsometricGame();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'isometric poc demo',
      theme: ThemeData(

        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: Stack(
          children: [
            GameWidget(game: _game, backgroundBuilder: (BuildContext context) => Container(color: Colors.cyan,),),
            Align(
              alignment: Alignment.bottomRight,
              child: NavigationKeys(onDirectionChanged: _game.onArrowKeyChanged,),
            )
          ],),
    );
  }
}
