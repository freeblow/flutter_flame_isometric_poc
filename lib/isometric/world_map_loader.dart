

import 'dart:ui';

import 'package:flame/cache.dart';
import 'package:flame/components.dart';
import 'package:flame/sprite.dart';

import 'isometric_game_world.dart';
import 'isometric_tile_map.dart';

class IsometricMapLoader{

  static const scale = 2.0;
  static const srcTileSize = 32.0;
  static const destTileSize = scale * srcTileSize;

  static const halfSize = true;
  static const tileHeight = scale * (halfSize ? 8.0 : 16.0);
  static const suffix = halfSize ? '-short' : '';

  static Future<IsometricTileMapComponent>  load_world1(Images images) async {

    final topLeft = Vector2.all(200);

    final tilesetImage = await images.load('tile_maps/single_layer/world_tile_1.png');
    Vector2 placeItemSize = Vector2.random();
    placeItemSize.setValues(51, 56);
    final tileset = SpriteSheet(
      image: tilesetImage,
      srcSize: placeItemSize,
    );
    final matrix = [
      [0, 0, 0, 0, 0, 0, 0],
      [0, 1, 1, 1, 1, 1, 0],
      [0, 1, 1, 1, 1, 1, 0],
      [0, 1, 1, 1, 1, 1, 0],
      [0, 1, 1, 1, 1, 1, 0],
      [0, 1, 1, 1, 1, 1, 0],
      [0, 0, 0, 0, 0, 0, 0]
    ];

    return IsometricTileMapComponent(
      tileset,
      matrix,
      destTileSize: placeItemSize,
      tileHeight: null,
      position: topLeft,
    );

  }


  static Future<IsometricTileMapComponent>  load_world2(Images images) async {

    final topLeft = Vector2.all(200);

    final tilesetImage = await images.load('tile_maps/single_layer/world_tile_3.png');
    Vector2 placeItemSize = Vector2.random();
    placeItemSize.setValues(51, 56);
    final tileset = SpriteSheet(
      image: tilesetImage,
      srcSize: placeItemSize,
    );
    // final matrix = [
    //   [1, 1, 1, 1, 1, 1, 1],
    //   [1, 0, 0, 0, 0, 0, 1],
    //   [1, 0, 0, 0, 0, 0, 1],
    //   [1, 0, 0, 0, 0, 0, 1],
    //   [1, 0, 0, 0, 0, 0, 1],
    //   [1, 0, 0, 0, 0, 0, 1],
    //   [1, 1, 1, 1, 1, 1, 1]
    // ];

    final matrix = [
      [0, 0, 0, 0, 0, 0, 0],
      [0, 1, 1, 1, 1, 1, 0],
      [0, 1, 1, 1, 1, 1, 0],
      [0, 1, 1, 1, 1, 1, 0],
      [0, 1, 1, 1, 1, 1, 0],
      [0, 1, 1, 1, 1, 1, 0],
      [0, 0, 0, 0, 0, 0, 0]
    ];

    return IsometricTileMapComponent(
      tileset,
      matrix,
      destTileSize: placeItemSize,
      tileHeight: null,
      position: topLeft,
    );
  }

  static Future<IsometricTileMapComponent>  load_world3(Images images) async {
    final topLeft = Vector2.all(0);

    final tilesetImage = await images.load('tile_maps/single_layer/world_tile_2.png');
    Vector2 placeItemSize = Vector2.random();
    placeItemSize.setValues(51, 56);
    final tileset = SpriteSheet(
      image: tilesetImage,
      srcSize: placeItemSize,
    );
    // final matrix = [
    //   [1, 1, 1, 1, 1, 1, 1],
    //   [1, 0, 0, 0, 0, 0, 1],
    //   [1, 0, 0, 0, 0, 0, 1],
    //   [1, 0, 0, 0, 0, 0, 1],
    //   [1, 0, 0, 0, 0, 0, 1],
    //   [1, 0, 0, 0, 0, 0, 1],
    //   [1, 1, 1, 1, 1, 1, 1]
    // ];

    final matrix = [
      [0, 0, 0, 0, 0, 0, 0],
      [0, 1, 1, 1, 1, 1, 0],
      [0, 1, 2, 2, 2, 1, 0],
      [0, 1, 2, 3, 2, 1, 0],
      [0, 1, 2, 2, 2, 1, 0],
      [0, 1, 1, 1, 1, 1, 0],
      [0, 0, 0, 0, 0, 0, 0]
    ];

    // return IsometricGameWorld(
    //     tileset,
    //     matrix,
    //     destTileSize: placeItemSize,
    //     tileHeight: null,
    //     position: topLeft,);

    return IsometricTileMapComponent(
      tileset,
      matrix,
      destTileSize: placeItemSize,
      tileHeight: null,
      position: topLeft,
    );
  }

}