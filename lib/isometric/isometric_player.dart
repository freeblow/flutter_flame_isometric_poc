
import 'dart:math';

import 'package:flame/components.dart';
import 'package:flame/effects.dart';
import 'package:flame/sprite.dart';
import 'package:flutter_flame_isometric_poc/isometric/common/isometric_directions.dart';
import 'package:flutter_flame_isometric_poc/isometric/common/isometric_move_unit.dart';

class IsometricPlayer extends SpriteAnimationComponent  with HasGameRef{
  IsometricPlayer({required this.direction}) : super(size: Vector2.all(128.0), anchor: Anchor.center);

  IsometricDirection direction = IsometricDirection.none;

  late final SpriteAnimation _walkingRightAnimation;
  late final SpriteAnimation _walkingLeftAnimation;
  late final SpriteAnimation _walkingUpAnimation;
  late final SpriteAnimation _walkingDownAnimation;
  late  SpriteAnimation _idleAnimation;

  final double _animationSpeed = .15;

  Future<void> _loadAnimations() async{
    final spriteSheetRight = SpriteSheet.fromColumnsAndRows(
        image: await gameRef.images.load('tile_maps/knight_ne.png'),
        columns: 8,
        rows: 1);
    final spriteSheetLeft = SpriteSheet.fromColumnsAndRows(
        image: await gameRef.images.load('tile_maps/knight_sw.png'),
        columns: 8,
        rows: 1);
    final spriteSheetUp = SpriteSheet.fromColumnsAndRows(
        image: await gameRef.images.load('tile_maps/knight_nw.png'),
        columns: 8,
        rows: 1);
    final spriteSheetBottom = SpriteSheet.fromColumnsAndRows(
        image: await gameRef.images.load('tile_maps/knight_se.png'),
        columns: 8,
        rows: 1);

    _walkingRightAnimation = spriteSheetRight.createAnimation(row: 0, stepTime: _animationSpeed, from:  0, to:  7);
    _walkingLeftAnimation = spriteSheetLeft.createAnimation(row: 0, stepTime: _animationSpeed, from: 0, to: 7);
    _walkingUpAnimation = spriteSheetUp.createAnimation(row: 0, stepTime: _animationSpeed, from:  0, to:  7);
    _walkingDownAnimation = spriteSheetBottom.createAnimation(row: 0, stepTime: _animationSpeed, from: 0, to: 7);
    _idleAnimation = _walkingDownAnimation;
  }

  @override
  Future<void> onLoad() async{
    super.onLoad();
    await _loadAnimations().then((_) => {animation = _idleAnimation});
  }

  @override
  void update(double dt) {
    // TODO: implement update
    super.update(dt);
    updatePostion(dt);
  }

  //update player component real time position(isometric)
  void updatePostion(double dt){
    Vector2 moveStep = IsometricMoveUnit.move(1);
    switch(direction){
      case IsometricDirection.up:
        animation = _walkingUpAnimation;
        _idleAnimation = animation!;
        position.y -= moveStep.y;
        position.x -= moveStep.x;
        break;
      case IsometricDirection.down:
        animation = _walkingDownAnimation;
        _idleAnimation = animation!;
        position.y += moveStep.y;
        position.x += moveStep.x;
        break;
      case IsometricDirection.left:
        animation = _walkingLeftAnimation;
        _idleAnimation = animation!;
        position.y += moveStep.y;
        position.x -= moveStep.x;
        break;
      case IsometricDirection.right:
        animation = _walkingRightAnimation;
        _idleAnimation = animation!;
        position.y -= moveStep.y;
        position.x += moveStep.x;
        break;
      case IsometricDirection.none:
        animation = _idleAnimation;
        break;
    }
  }

}