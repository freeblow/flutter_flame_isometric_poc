

import 'dart:math';
import 'dart:ui';

import 'package:flame/events.dart';
import 'package:flame/experimental.dart';
import 'package:flame/game.dart';
import 'package:flame/src/experimental/geometry/shapes/shape.dart';
import 'package:flutter_flame_isometric_poc/isometric/common/isometric_directions.dart';
import 'package:flutter_flame_isometric_poc/isometric/isometric_game_world.dart';
import 'package:flutter_flame_isometric_poc/isometric/isometric_player.dart';

class IsometricGame extends FlameGame{
  IsometricGameWorld _gameWorld = IsometricGameWorld();
  IsometricPlayer _player = IsometricPlayer(direction: IsometricDirection.none);

  @override
  Future<void> onLoad() async{
    super.onLoad();
    add(_gameWorld);

    _gameWorld.addRole(_player);

    var gameSize = _gameWorld.gameRef.size;

    // print("view port size = ${camera.viewport.size}");
    // print("game ref size = ${gameSize}");

    _player.position = Vector2(camera.viewport.size.x / 4, camera.viewport.size.y / 4);
    camera.follow(_player,snap: true);
    // camera.setBounds(Rectangle.fromRect(Rect.fromLTRB(190, -50, 810, 50)));

  }


  onArrowKeyChanged(IsometricDirection direction){
    //
    _player.direction = direction;
  }

  //
  // //zoom method
  // void clampZoom(double zoom) {
  //   camera.viewfinder.zoom = zoom.clamp(0.05, 3.0);
  // }
  //
  // static const zoomPerScrollUnit = 0.02;
  //
  // @override
  // void onScroll(PointerScrollInfo info) {
  //   print("onScroll ------ !");
  //   clampZoom(info.scrollDelta.global.y.sign * zoomPerScrollUnit);
  // }
  //
  // late double startZoom;
  //
  // @override
  // void onScaleStart(_) {
  //   print("onScaleStart ------ !");
  //   startZoom = camera.viewfinder.zoom;
  // }
  //
  // @override
  // void onScaleUpdate(ScaleUpdateInfo info) {
  //   print("onScaleUpdate ------ !");
  //   final currentScale = info.scale.global;
  //   if (!currentScale.isIdentity()) {
  //     clampZoom(startZoom * currentScale.y);
  //   } else {
  //     final delta = info.delta.global;
  //     camera.viewfinder.position.translate(-delta.x, -delta.y);
  //   }
  // }



}