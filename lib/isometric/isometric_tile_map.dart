import 'dart:ui';

import 'package:flame/components.dart';
import 'package:flame/events.dart';
import 'package:flame/extensions.dart';
import 'package:flame/game.dart';
import 'package:flame/input.dart';
import 'package:flame/sprite.dart';
import 'package:flutter_flame_isometric_poc/isometric/isometric_game_world.dart';
import 'package:flutter_flame_isometric_poc/isometric/world_map_loader.dart';

class IsometricTileMapExample extends FlameGame with MouseMovementDetector, VerticalDragDetector {
  static const String description = '''
    Shows an example of how to use the `IsometricTileMapComponent`.\n\n
    Move the mouse over the board to see a selector appearing on the tiles.
  ''';

  late IsometricTileMapComponent base;
  late RoleComponent roleComponent;

  IsometricTileMapExample();

  @override
  Future<void> onLoad() async {

   IsometricMapLoader.load_world3(images)
     .then((value){
       base = value;
       base.position = Vector2(size.x/2 - base.size.x/2, size.y/2 - base.size.y/2);
       add(base);

       loadWorlMapFinished();
     });
  }

  Future<void> loadWorlMapFinished() async {
    Vector2 topLeft = base.position;

    final selectorImage = await images.load('tile_maps/sample1/role_tile.png');
    roleComponent = RoleComponent(Vector2(37, 69), selectorImage);
    final sPos = Vector2.all(0);
    // sPos.setValues(32, 0);
    roleComponent.position.setFrom(sPos);
    add(roleComponent);

  }

  // @override
  // void render(Canvas canvas) {
  //   super.render(canvas);
  //   canvas.renderPoint(topLeft, size: 5, paint: originColor);
  //   canvas.renderPoint(
  //     topLeft.clone()..y -= tileHeight,
  //     size: 5,
  //     paint: originColor2,
  //   );
  // }

  @override
  void onMouseMove(PointerHoverInfo info) {
    // final screenPosition = info.eventPosition.widget;
    // final block = base.getBlock(screenPosition);
    // selector.show = base.containsBlock(block);
    // selector.position.setFrom(topLeft + base.getBlockRenderPosition(block));
  }

  @override
  void onVerticalDragDown(DragDownInfo info) {
    print("onVerticalDragDown ------ !");
  }
  @override
  void onVerticalDragStart(DragStartInfo info) {
    print("onVerticalDragStart ------ !");
  }
  @override
  void onVerticalDragUpdate(DragUpdateInfo info) {
    print("onVerticalDragUpdate ------ !");

  }

  @override
  void onVerticalDragEnd(DragEndInfo info) {
    print("onVerticalDragEnd ------ !");
  }
  @override
  void onVerticalDragCancel() {
    print("onVerticalDragCancel ------ !");
  }
}

class RoleComponent extends SpriteComponent {
  bool show = true;

  RoleComponent(Vector2 size, Image image)
      : super(
          sprite: Sprite(image, srcSize: Vector2.all(32.0)),
          size: size,
        );

  @override
  void render(Canvas canvas) {
    if (!show) {
      return;
    }

    super.render(canvas);
  }
}
