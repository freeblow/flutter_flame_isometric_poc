import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flame_isometric/flame_isometric.dart';
import 'package:flame_isometric/custom_isometric_tile_map_component.dart';
import 'package:flutter/material.dart';

class IsometricMutilLayerGame extends FlameGame with HasGameRef {


  @override
  Future<void> onLoad() async {
    super.onLoad();
    final gameSize = gameRef.size;

    // final flameIsometric = await FlameIsometric.create(
    //     tileMap: ['tile_maps/tile_map.png'],
    //     tsxList: ['images/tile_maps/multi_layer/multi_layer.tsx'],
    //     tmx: 'images/tile_maps/multi_layer/multi_layer.tmx'
    // );
    //
    // for (var i = 0; i < flameIsometric.layerLength; i++) {
    //   print("layer tileSet Count = ${flameIsometric.tileset.columns}");
    //   add(
    //     IsometricTileMapComponent(
    //       flameIsometric.tileset,
    //       flameIsometric.renderMatrixList[i],
    //       destTileSize: flameIsometric.srcTileSize,
    //       position:
    //           Vector2(gameSize.x / 2, flameIsometric.tileHeight.toDouble()),
    //     ),
    //   );
    // }


    final flameIsometric = await FlameIsometric.create(
        tileMap: ['tile_maps/tile_map.png'],
        tsxList: ['images/tile_maps/multi_layer/multi_layer.tsx'],
        tmx: 'images/tile_maps/multi_layer/multi_layer.tmx'
    );

    for (var renderLayer in flameIsometric.renderLayerList) {
      add(
        CustomIsometricTileMapComponent(
          renderLayer.spriteSheet,
          renderLayer.matrix,
          destTileSize: flameIsometric.srcTileSize,
          position:
          Vector2(gameSize.x / 2, flameIsometric.tileHeight.toDouble()),
        ),
      );

    }


  }
}