import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_flame_isometric_poc/isometric/common/isometric_directions.dart';
import 'arrow_key.dart';

class NavigationKeys extends StatefulWidget {
  final ValueChanged<IsometricDirection>? onDirectionChanged;

  const NavigationKeys({Key? key, required this.onDirectionChanged}): super(key: key);

  @override
  State<NavigationKeys> createState() => _NavigationKeysState();


}

class _NavigationKeysState extends State<NavigationKeys> {

  IsometricDirection direction = IsometricDirection.none;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200,
      width: 120,
      child: Column(
        children: [
          ArrowKey(
              icons: Icons.keyboard_arrow_up,
              onTapDown: (det){
                updateDirection(IsometricDirection.up);
              },
              onTapUp: (dets){
                updateDirection(IsometricDirection.none);
              },
              onLongPressDown: (){
                updateDirection(IsometricDirection.up);
              },
              onLongPressEnd: (dets){
                updateDirection(IsometricDirection.none);
              }),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ArrowKey(
                  icons: Icons.keyboard_arrow_left,
                  onTapDown: (det){
                    updateDirection(IsometricDirection.left);
                  },
                  onTapUp: (dets){
                    updateDirection(IsometricDirection.none);
                  },
                  onLongPressDown: (){
                    updateDirection(IsometricDirection.left);
                  },
                  onLongPressEnd: (dets){
                    updateDirection(IsometricDirection.none);
                  }),
              ArrowKey(
                  icons: Icons.keyboard_arrow_right,
                  onTapDown: (det){
                    updateDirection(IsometricDirection.right);
                  },
                  onTapUp: (dets){
                    updateDirection(IsometricDirection.none);
                  },
                  onLongPressDown: (){
                    updateDirection(IsometricDirection.right);
                  },
                  onLongPressEnd: (dets){
                    updateDirection(IsometricDirection.none);
                  })
            ],
          ),
          ArrowKey(icons: Icons.keyboard_arrow_down,
              onTapDown: (det){
                updateDirection(IsometricDirection.down);
              },
              onTapUp: (dets){
                updateDirection(IsometricDirection.none);
              },
              onLongPressDown: (){
                updateDirection(IsometricDirection.down);
              },
              onLongPressEnd: (dets){
                updateDirection(IsometricDirection.none);
              })
        ],
      ),
    );
  }

  void updateDirection(IsometricDirection newDirection){
    direction = newDirection;
    widget.onDirectionChanged!(direction);
  }
}
