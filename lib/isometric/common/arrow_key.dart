import 'package:flutter/cupertino.dart';

class ArrowKey extends StatelessWidget {
  const ArrowKey({super.key, required this.icons, required this.onTapDown, required this.onTapUp, required this.onLongPressDown, required this.onLongPressEnd});

  final IconData icons;
  final Function(TapDownDetails) onTapDown;
  final Function(TapUpDetails) onTapUp;
  final Function() onLongPressDown;
  final Function(LongPressEndDetails) onLongPressEnd;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: onTapDown,
      onTapUp: onTapUp,
      onLongPress: onLongPressDown,
      onLongPressEnd: onLongPressEnd,
      child: Container(
        margin: const EdgeInsets.all(8),
        decoration: BoxDecoration(
            color: const Color(0x88ffffff),
            borderRadius: BorderRadius.circular(60)
        ),
        child: Icon(
          icons,
          size: 42,
        ),
      ),
    );
  }
}
