

import 'dart:math';

import 'package:flame/components.dart';

class IsometricMoveUnit{
  static Vector2 move(double mValue){
    return Vector2( mValue * sqrt(3) / 2 , mValue / 2);
  }
}