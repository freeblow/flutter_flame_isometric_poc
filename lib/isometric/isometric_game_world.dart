import 'package:flame/components.dart';
import 'package:flame/events.dart';
import 'package:flame_isometric/custom_isometric_tile_map_component.dart';
import 'package:flame_isometric/flame_isometric.dart';
import 'package:flutter_flame_isometric_poc/isometric/world_map_loader.dart';

class IsometricGameWorld extends PositionComponent with HasGameRef, DragCallbacks{
  IsometricGameWorld();

  CustomIsometricTileMapComponent? _lastLayer;

  CustomIsometricTileMapComponent get lastLayer => _lastLayer!;

  PositionComponent? _roleComponent;


  late Vector2 startPosition = Vector2(0, 0);


  @override
  Future<void> onLoad() async {
    // TODO: implement onLoad
    super.onLoad();

    size = gameRef.size;
    final gameSize = gameRef.size;

    final flameIsometric = await FlameIsometric.create(
        tileMap: ['tile_maps/tile_map.png'],
        tsxList: ['images/tile_maps/multi_layer/multi_layer.tsx'],
        tmx: 'images/tile_maps/multi_layer/multi_layer.tmx'
    );


    for (var renderLayer in flameIsometric.renderLayerList) {
      _lastLayer = CustomIsometricTileMapComponent(
        renderLayer.spriteSheet,
        renderLayer.matrix,
        destTileSize: flameIsometric.srcTileSize,
        position:
        Vector2(gameSize.x / 2, flameIsometric.tileHeight.toDouble()),
      );
      add(_lastLayer!);
    }

    if(_roleComponent != null && !_lastLayer!.contains(_roleComponent!)){
        _lastLayer!.add(_roleComponent!);
    }

  }

  void addRole(Component role){
    _roleComponent = role as PositionComponent?;
    if(_roleComponent != null && _lastLayer != null && !_lastLayer!.contains(_roleComponent!)){
      _lastLayer!.add(_roleComponent!);
      return;
    }
  }

  @override
  void onDragStart(DragStartEvent event) {
    // TODO: implement onDragStart
    super.onDragStart(event);

    startPosition = position;

    print("onDragStart ----- !");
  }

  @override
  void onDragUpdate(DragUpdateEvent event) {
    // TODO: implement onDragUpdate
    super.onDragUpdate(event);

    print("event == ${event}");

    position = position + event.delta ;

    print("onDragUpdate ----- !");
  }

  @override
  void onDragEnd(DragEndEvent event) {
    // TODO: implement onDragEnd
    super.onDragEnd(event);
    print("onDragEnd ----- !");

    // startPosition = position;
  }

  @override
  void onDragCancel(DragCancelEvent event) {
    // TODO: implement onDragCancel
    super.onDragCancel(event);

    // startPosition = position;

    print("onDragCancel ----- !");
  }




}